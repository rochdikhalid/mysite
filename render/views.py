from django.shortcuts import render



def index(request):
    return render(request, 'render/index.html', {})

def about(request):
    return render(request, 'render/about.html', {})
